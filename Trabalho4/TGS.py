#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 4 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 29/05/18                  #
#      Implementação:                                       #
#                     Servidor TGS                          #
#                                                           #
#                                                           #
#                                                           #
#############################################################


import socket
import sys
from _thread import *
from Crypto.Cipher import DES3
import time
import numpy as np
import string
import hashlib


HOST = ''
PORT = 7777
BUFFER_SIZE = 1024
CONEXAO = 10
salt = 895807183804302924969748361977
listKCS = {}
listKCTGS = {}
listKS = {}



def hashGenerator(entrada, salt):

    hash = entrada + str(salt).encode()
    base = hashlib.sha256(hash).hexdigest()
    return base


def cipherKCS(usuario):

    try:
        npk = str(np.random.bytes(16))
        key = hashGenerator(npk.encode(), salt)
        listKCS[usuario] = key

    except Exception as e:
        print("Erro ao criar o arquivo kcs: ", str(e))


def cipherKS(usuario):

    try:
        saida = open('ks.txt', 'w')
        # key = Random.new().read(16)
        npk = str(np.random.bytes(16))
        key = hashGenerator(npk.encode(), salt)

        resp = usuario + "," + key
        listKS[usuario] = key
        saida.writelines(resp)
        saida.close()
        time.sleep(0.05)
    except Exception as e:
        print("Erro ao gravar o arquivo ks.txt: ", str(e))


def createCipher(user):
    try:
        if user in listKCTGS:
            time.sleep(0.05)
            key = listKCTGS[user]
            print("Imprimindo a key: ",str(key))
            key32 = key.ljust(24)[:24]
            iv = key.ljust(16)[35:43]
            cipher = DES3.new(key32, DES3.MODE_CFB, iv)
            return cipher

    except Exception as e:
        print("Erro a criar cipher kctgs: ", str(e))


def encryptText(mensagem, cipher):
    cipher = cipher
    space = '        '
    text = mensagem
    plaintext = space + text
    msg = cipher.encrypt(plaintext.encode(encoding='utf_8'))
    print("encrypt: " + str(msg))
    return msg


def decryptText(mensagem, cipher):
    cipher = cipher
    msg = cipher.decrypt(mensagem)
    print("Msg decrypt: ", msg[8:])
    return msg[8:]


def serverThread(conn):

    msg = "Bem vindo ao Servidor TGS"
    conn.send(msg.encode())
    listKCTGS.clear()
    listKCS.clear()
    listKS.clear()
    while True:
        try:
            conn.settimeout(15)
            time.sleep(0.05)
            user = "astgs"
            data = conn.recv(BUFFER_SIZE)
            decifrado = decifraTicket(user, data)

            if len(decifrado) > 0:
                translator = str.maketrans('', '', string.punctuation)
                print("Ticket decifrado com sucesso!")
                msg = str(decifrado)
                #msg = msg.split('b')[1]
                msg = msg.split(",")
                usuario = msg[0]
                usuario =  usuario.split('b')[1]
                usuario = usuario.translate(translator)
                chave = msg[3]
                chave = chave.translate(translator)
                print("usuario: ", usuario)
                tempo = msg[2]
                listKCTGS[usuario] = chave
                cipher = createCipher(usuario)
                #cipherKCS(usuario)
                data = conn.recv(BUFFER_SIZE)
                print("data Recebida: ", data)

                if len(data) > 0:
                    print("Decifrando a Msg")
                    dec = decryptText(data, cipher)
                    a = str(dec)
                    print(a)
                    resp = geraResposta(usuario, a, cipher)
                    conn.send(resp)
                    time.sleep(0.05)
                    tick = geraTicket(usuario, tempo, a)
                    conn.send(tick)
                    break

            else:
                print("Problemas na conexão do TGS")
                break

            # if len(data) == 0:
            #     resp = "data == 0 no TGS"
            #     print(data)
            #     conn.send(resp.encode())
            #     break

        except conn.timeout as error:
            print("Timout TGS: ", str(error))
            break
    time.sleep(0.05)
    conn.close()


def decifraTicket(usuario, msg):

    listASTGS = {}
    try:
        userFile = open("ktgs.txt", 'r')
        for line in userFile:
            campo = line.split(",")
            listASTGS[campo[0]] = campo[1]
        userFile.close()
        time.sleep(0.05)

        if usuario in listASTGS:
            key = listASTGS[usuario]
            key32 = key.ljust(24)[:24]
            iv = key.ljust(16)[35:43]
            cipher = DES3.new(key32, DES3.MODE_CFB, iv)
            print("Msg Ticket: ", str(msg))
            resp = decryptText(msg, cipher)

        return resp
    except Exception as e:
        print("Erro ao abrir o arquivo ktgs.txt: ", str(e))


def geraResposta(usuario, msg, cipher):

    try:
        cipherKCS(usuario)
        if usuario in listKCS:
            nbr = listKCS[usuario]
            m = msg.split("b'")[1]
            m = m.replace(m[len(m)-1], "", 1)
            m = m.split(",")
            m2 = str(nbr) + "," + m[2] + "," + m[3]
            print("Resposta TGS: ", m2)
            resp = encryptText(m2, cipher)
        return resp

    except Exception as e:
        print("Erro ao acessar a lista kcs: ", str(e))


def geraTicket(user, tempo, msg):

    try:
        usuario = 'tgs'
        cipherKS(usuario)
        time.sleep(0.05)

        if usuario in listKS:
            try:
                key = listKS[usuario]
                key32 = key.ljust(24)[:24]
                iv = key.ljust(16)[35:43]
                cipher = DES3.new(key32, DES3.MODE_CFB, iv)
                m = msg.split("b'")[1]
                m = m.replace(m[len(m)-1], "", 1)
                m = m.split(",")
                m2 = user + "," + m[1] + "," + tempo + "," + str(listKCS[user])
                print("Msg Ticket: ", m2)
                resp = encryptText(m2, cipher)
                return resp

            except Exception as e:
                print("Erro ao gerar a cifra do geraTicket: ", str(e))
    except Exception as e:
        print("Erro ao abrir ks.txt no geraTicket: ", str(e))


def main():

    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Socket criado")

    try:
        tcp.bind((HOST, PORT))
    except socket.error:
        print("Binding failed")
        sys.exit

    print("Socket  bounded")
    tcp.listen(CONEXAO)

    print("Socket está pronto")
    print("Bem vindo ao Servidor TGS")
    while 1:
        conn, addr = tcp.accept()
        print("\nConectado com: " + addr[0] + ":" + str(addr[1]))
        print()
        start_new_thread(serverThread, (conn, ))

    conn.close()
    tcp.close()


if __name__ == "__main__":
    main()
