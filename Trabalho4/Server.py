#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 4 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 29/05/18                  #
#      Implementação:                                       #
#                     Servidor Serviços                     #
#                                                           #
#                                                           #
#                                                           #
#############################################################


import socket
import sys
from _thread import *
from Crypto.Cipher import DES3
import time
from time import strftime
import numpy as np
import string
import hashlib

HOST = ''
PORT = 8888
BUFFER_SIZE = 1024
CONEXAO = 10
listKCS = {}
serviceList = ('serv1', 'serv2', 'serv3', 'serv4')
salt = 895807183804302924969748361977


def hashGenerator(entrada, salt):

    hash = entrada + str(salt).encode()
    base = hashlib.sha256(hash).hexdigest()
    return base


def createCipher(user):

    try:
        if user in listKCS:
            time.sleep(0.05)
            key = listKCS[user]
            print("Imprimindo a key: ",str(key))
            key32 = key.ljust(24)[:24]
            iv = key.ljust(16)[35:43]
            cipher = DES3.new(key32, DES3.MODE_CFB, iv)
            return cipher
        else:
            print("Lista Vazia KCS!")
    except Exception as e:
        print("Erro ao gerar cifra kcs: ", str(e))


def encryptText(mensagem, cipher):
    cipher = cipher
    space = '        '
    text = mensagem
    plaintext = space + text
    msg = cipher.encrypt(plaintext.encode(encoding='utf_8'))
    print("encrypt: " + str(msg))
    return msg


def decryptText(mensagem, cipher):
    cipher = cipher
    msg = cipher.decrypt(mensagem)
    print("Msg decrypt: ", msg[8:])
    return msg[8:]


def serverThread(conn):
    msg = "Bem vindo ao Servidor de Serviços!"
    user = "tgs"
    conn.send(msg.encode())
    listKCS.clear()

    while True:
        try:
            conn.settimeout(15)
            data = conn.recv(BUFFER_SIZE)
            decifrado = decifraTicket(user, data)

            if len(decifrado) > 0:
                translator = str.maketrans('', '', string.punctuation)
                print("Ticket decifrado com sucesso!")
                msg = str(decifrado)
                msg = msg.split(",")
                usuario = msg[0]
                usuario = usuario.split('b')[1]
                usuario = usuario.translate(translator)
                tempo = msg[2]
                chave = msg[3]
                chave = chave.translate(translator)
                listKCS[usuario] = chave
                time.sleep(0.05)
                cipher = createCipher(usuario)

                data = conn.recv(BUFFER_SIZE)
                if len(data) > 0:
                    print("Decifrando a Msg")
                    dec = decryptText(data, cipher)
                    a = str(dec)
                    print(a)
                    resp = geraResposta(usuario, a, cipher, tempo)
                    time.sleep(0.05)
                    conn.send(resp)
                    time.sleep(0.05)
                    print()
                    break

                else:
                    print("Problemas na conexão do Servidor")

            if len(data) == 0:
                resp = "data == 0 no Servidor"
                print(resp)
                conn.send(resp.encode())
                break
        except conn.timeout as error:
            print("Timout Servidor: ", str(error))
            break
    conn.close()


def decifraTicket(usuario, msg):

    listAKS = {}
    try:
        userFile = open("ks.txt", 'r')
        for line in userFile:
            campo = line.split(",")
            listAKS[campo[0]] = campo[1]
        try:
            if usuario in listAKS:
                key = listAKS[usuario]
                key32 = key.ljust(24)[:24]
                iv = key.ljust(16)[35:43]
                cipher = DES3.new(key32, DES3.MODE_CFB, iv)
                print("Msg Ticket: ", str(msg))
                resp = decryptText(msg, cipher)
            time.sleep(0.05)
            userFile.close()
            return resp
        except Exception as e:
            print("Erro no decifraTicket: ", str(e))

    except Exception as e:
        print("Erro ao abrir o arquivo ks.txt: ", str(e))


def geraResposta(usuario, msg, cipher, tempo):

    tempo2 = strftime("%Y:%m:%d:%H:%M", time.gmtime())
    tempo = tempo.split(':')
    tempo2 = tempo2.split(':')

    r1 = int(tempo[0])*int(tempo[1])*int(tempo[2]) + int(tempo[3])*3600
    + int(tempo[4])*60
    r2 = int(tempo2[0])*int(tempo2[1])*int(tempo2[2]) + int(tempo2[3])*3600
    + int(tempo2[4])*60
    result = r1 - r2

    if usuario in listKCS and result <= 50:
        m = msg.split("b'")[1]
        m = m.replace(m[len(m)-1], "", 1)
        m = m.split(",")

        if m[1] in serviceList:
            m2 = m[1] + "," + m[3]
        else:
            m2 = "Serviço Inválido!"

    elif result > 5:
        m2 = "Tempo do Ticket Esgotado!"
    else:
        m2 = "Usuário não encontrado!"

    print("Resposta Servidor: ", m2)
    resp = encryptText(m2, cipher)
    return resp


def main():

    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Socket criado")

    try:
        tcp.bind((HOST, PORT))
    except socket.error:
        print("Binding failed")
        sys.exit

    print("Socket  bounded")
    tcp.listen(CONEXAO)

    print("Socket está pronto")
    print("Bem vindo ao Servidor de Serviços")
    while 1:
        conn, addr = tcp.accept()
        print("\nConectado com: " + addr[0] + ":" + str(addr[1]))
        print()
        start_new_thread(serverThread, (conn, ))

    conn.close()
    tcp.close()


if __name__ == "__main__":
    main()
