#!/usr/bin/env python3

#############################################################
#                                                           #
#                                                           #
#      Trabalho 5 de Segurança e Auditoria de Sistemas      #
#      Autor: Vinícius Sandri Diaz                          #
#                                                           #
#                                                           #
#      data da última modificação: 27/06/18                 #
#      Implementação:                                       #
#                     Servidor Proxy                        #
#                                                           #
#                                                           #
#                                                           #
#############################################################

import socket
import sys
from _thread import *
import signal


TIMEOUT = 2
HOST = ''
PORT = 6666
BUFFER_SIZE = 4096
CONEXAO = 3

filename= open('files/resposta.html', 'r')
fileResp = filename.read(BUFFER_SIZE)
filename.close()

def signal_handler(signal, frame):
    print('\nStop: pressing the CTRL+C!')
    sys.exit(0)


def proxyThread(conn, addr, data):
    
    try:
        print("Requisição http Completa")
        print(data)
        linha1 = data.split('\n')[0]
        
        if 'monitorando' in linha1:
            flag = False
        else:
            flag = True
       
       # get url        
        url = linha1.split(' ')[1]        
        http_pos = url.find("://") 
        
        print("request.split: ", linha1)
        print("url          : ", url)
        print("http_pos     : ", http_pos)
        
        try:
        
            if (http_pos==-1):
                temp = url
            else:
                temp = url[(http_pos+3):] # get the rest of url
            # Para encontrar a porta
            port_pos = temp.find(":") 
            
        except Exception as e:
            print("Erro 1: " + str(e))
            pass    

        # Para encontrar o fim web server
        try:
            webserver_pos = temp.find("/")
            if webserver_pos == -1:
                webserver_pos = len(temp)
        except Exception as e:
            print("Erro 2:" + str(e))
            pass  
    
        try:
            webserver = ""
            port = -1
            if (port_pos == -1 or webserver_pos < port_pos):
                # Porta padrão
                port = 80 
                webserver = temp[:webserver_pos] 
                
            else: 
                # Caso não seja a porta padrão
                port = int((temp[(port_pos+1):])[:webserver_pos - port_pos-1])
                webserver = temp[:port_pos] 
           
            webs = socket.gethostbyname(webserver)
            print("port_pos     : ", port_pos)
            print("webserver_pos: ", webserver_pos)
            print("temp         : ", temp)
            print("port         : ", port)
            print("Webserver    : ", webserver)             
            print("IP           : ", webs)
            
            if flag:
                proxyResponse(webserver, port, conn, data, addr)
            
            else:
                print("Acesso Negado: Acesso Negado!!!")
                conn.send(fileResp.encode('utf-8'))
                conn.close()
                  
        except Exception as e:
            print("Erro 3: " + str(e))
            pass  
    
    except Exception as e:
        print("Erro no proxyThread: " + str(e))
        pass


def proxyResponse(webserver, port, conn, data, addr):
    
    try: 
        dest =  (webserver, port)
        tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp.connect(dest)
        tcp.settimeout(TIMEOUT)
        tcp.send(data.encode())
    
        while 1: 
            reply= tcp.recv(BUFFER_SIZE) 
            
            if(len(reply) > 0):
                conn.send(reply)
                data_size = len(reply)
                dar = float(data_size/1024)
                print("Resp. Proxy  : " + (str(addr[0])) + " => " + format(dar, '.2f') + " KB")
            else:
                break
            
    except Exception as e:
        print("Erro Response: " + str(e))
        pass  
    
        
    tcp.close()
    conn.close()


def main():

    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    signal.signal(signal.SIGINT, signal_handler)
    
    print("Socket criado")

    try:
        tcp.bind((HOST, PORT))
    except socket.error:
        print("Binding failed")
        sys.exit

    print("Socket  bounded")
    tcp.listen(CONEXAO)

    print("Socket está pronto")
    while 1:
        conn, addr = tcp.accept()
        conn.settimeout(TIMEOUT)
        data = conn.recv(BUFFER_SIZE) 
        print("Conectado com: " + addr[0] + ":" + str(addr[1]))
        start_new_thread(proxyThread, (conn, addr, data.decode()))

    conn.close()
    tcp.close()


if __name__ == "__main__":
    main()
